# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm/configs/hisi_hi6620oem_defconfig

pkgname="linux-huawei-sophia"
pkgver=3.0.8
pkgrel=0
pkgdesc="Huawei P7-L10 kernel fork"
arch="armv7"
_carch="arm"
_flavor="huawei-sophia"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev devicepkg-dev gcc4 gcc4-armv7 lz4"

# Compiler: latest GCC from Alpine
#HOSTCC="${CC:-gcc}"
#HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Compiler: this kernel was only tested with GCC4. Feel free to make a merge
# request if you find out that it is booting working with newer GCCs as
# well. See <https://postmarketos.org/vendorkernel> for instructions.
if [ "${CC:0:5}" != "gcc4-" ]; then
	CC="gcc4-$CC"
	HOSTCC="gcc4-gcc"
	CROSS_COMPILE="gcc4-$CROSS_COMPILE"
fi

# Source
_repository="hwp6s-kernel"
_commit="062d3b4929ca230205591c377519b8c1962c3e19"
_config="config-${_flavor}.${arch}"
source="
	$pkgname-$_commit.tar.gz::https://github.com/Kostyan-nsk/${_repository}/archive/${_commit}.tar.gz
	$_config
	tty-vt-Fix-the-memory-leak-in-visual_init.diff
	TWEAKED_0001-ARM-7668-1-fix-memset-related-crashes-caused-by-rece.patch
	
	gcc7-give-up-on-ilog2-const-optimizations.patch
	gcc8-fix-put-user.patch
	patch-makefile.patch
"
#TWEAKED_0001-ARM-7670-1-fix-the-memset-fix.patch
builddir="$srcdir/${_repository}-${_commit}"

prepare() {
	default_prepare
	downstreamkernel_prepare "$srcdir" "$builddir" "$_config" "$_carch" "$HOSTCC"
}

build() {
	unset LDFLAGS
	grep -rl '\-mcpu\=cortex\-a9' . | xargs sed -i 's/\-mcpu\=cortex\-a9//g'

	make ARCH="$_carch" CC="${CC:-gcc}"\
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" -j9
}

package() {
	# kernel.release
	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# zImage (find the right one)
	cd "$builddir/arch/$_carch/boot"
	_target="$pkgdir/boot/vmlinuz-$_flavor"
	for _zimg in zImage-dtb Image.gz-dtb *zImage Image; do
		[ -e "$_zimg" ] || continue
		msg "zImage found: $_zimg"
		install -Dm644 "$_zimg" "$_target"
		break
	done
	if ! [ -e "$_target" ]; then
		error "Could not find zImage in $PWD!"
		return 1
	fi
}

sha512sums="8e4305e3a2517100a337359f1f0367086dbc1dc8b6bc0ca3461353423a55a9a497666bd44bbd32d5871298fe1b184e3f8f79af1563a6bb196e8781c7b3560304  linux-huawei-sophia-062d3b4929ca230205591c377519b8c1962c3e19.tar.gz
5a74f4e5f9e22a87142ad40d44706ef19e1e7235ce51c20881abaf9379fa14482b66770c6966ac78fd6c12125d66afcd1b461efd50050bd8168f6893062cd0fe  config-huawei-sophia.armv7
5d5594bf7a861bf6d85a11fbd1cb457b99ec9002ab4ba71861f980eaf19a40df5d5df671597ee383dac913c37b4c41f7492bdff746753c8482b1ae182796f7d9  tty-vt-Fix-the-memory-leak-in-visual_init.diff
890e51553bc0f8bd4022178c1445161a100c867c593f3feeb3a5880ad40eaf742668932aa09fc0118568fd8a27f10bd0d5650573297fe60f7bc6a4ded8b085bb  TWEAKED_0001-ARM-7668-1-fix-memset-related-crashes-caused-by-rece.patch
77eba606a71eafb36c32e9c5fe5e77f5e4746caac292440d9fb720763d766074a964db1c12bc76fe583c5d1a5c864219c59941f5e53adad182dbc70bf2bc14a7  gcc7-give-up-on-ilog2-const-optimizations.patch
197d40a214ada87fcb2dfc0ae4911704b9a93354b75179cd6b4aadbb627a37ec262cf516921c84a8b1806809b70a7b440cdc8310a4a55fca5d2c0baa988e3967  gcc8-fix-put-user.patch
c8ef2595a377a54b84fe1b7abbad18f2ad00784b5a479f82f3b6b85e26b3620f0924dc2df7270c865c14e4abe9a00a1382e2fa11c704cc65f2aa76316e71d52d  patch-makefile.patch"
